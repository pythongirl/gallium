use std::sync::mpsc::channel;
use std::time::Duration;

use handlebars::Handlebars;
use notify::{DebouncedEvent, RecursiveMode, Watcher};

use crate::render;

pub fn watch_files(renderer_paths: &render::RendererPaths) {
    let (input_dir, template_dir, _output_dir) = renderer_paths.get_refs();

    let mut handlebars = Handlebars::new();
    handlebars.register_helper("markdown", Box::new(crate::markdown_helper::markdown_block));

    let (tx, rx) = channel();

    let mut watcher = notify::watcher(tx, Duration::from_millis(500)).unwrap();

    watcher.watch(input_dir, RecursiveMode::Recursive).unwrap();
    watcher
        .watch(template_dir, RecursiveMode::Recursive)
        .unwrap();

    match render::render_site(renderer_paths, &mut handlebars) {
        Ok(()) => {
            println!("Rendered site.");
        }
        Err(e) => {
            eprintln!("Error when rerendering site: {}", e);
        }
    }

    loop {
        match rx.recv() {
            Ok(event) => {
                match event {
                    DebouncedEvent::Error(_, _) | DebouncedEvent::Rescan => {
                        // Error and Rescan are not file changes
                    }
                    DebouncedEvent::NoticeRemove(_) | DebouncedEvent::NoticeWrite(_) => {
                        // Inore the notice events because they serve special pourposes.
                    }
                    DebouncedEvent::Chmod(_) => {
                        // A chmod doesn't matter
                    }
                    DebouncedEvent::Create(_)
                    | DebouncedEvent::Write(_)
                    | DebouncedEvent::Remove(_)
                    | DebouncedEvent::Rename(_, _) => {
                        println!("Change detected.");

                        // TODO: Add support for differential rendering based on the file change

                        match render::render_site(renderer_paths, &mut handlebars) {
                            Ok(()) => {
                                println!("Rerendered site.");
                            }
                            Err(e) => {
                                eprintln!("{}", e);
                            }
                        }
                    }
                }
            }
            Err(e) => {
                eprintln!("Watch error: {}", e);
            }
        }
    }
}
