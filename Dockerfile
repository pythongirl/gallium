FROM rust:1.38

WORKDIR /usr/src/gallium
COPY . .

RUN cargo install --path .
